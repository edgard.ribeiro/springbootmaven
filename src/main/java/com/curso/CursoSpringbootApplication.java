package com.curso;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoSpringbootApplication implements CommandLineRunner{
	
	

	public static void main(String[] args) {
		SpringApplication.run(CursoSpringbootApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		System.out.println("Inicindo o RUN");
		
		for(int i = 1; i <= 10; i++) {
			System.out.println(i);
		}
		
		Curso c = new Curso();
		c.mostraDados();
									
	}

}
